from setuptools import find_packages, setup
from distutils.dist import Distribution


class CustomDistribution(Distribution):
    def __init__(self, *args, **kwargs):
        import sourcehook

        sourcehook.patch_distutils().__enter__()
        super().__init__(*args, **kwargs)


dep = "sourcehook==0.0.4"

setup(
    name="sourcehook_example",
    version="0.0.1",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    install_requires=[dep],
    setup_requires=[dep],
    distclass=CustomDistribution,
)
