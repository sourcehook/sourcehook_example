from sourcehook_example import module_1, module_2


def test_sourcehook():
    assert (
        str(module_1.B)
        == "<class 'sourcehook_example.module_1._global_scope.<locals>.B'>"
    )
    assert str(module_2.B) == "<class 'sourcehook_example.module_2.B'>"
